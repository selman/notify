# -*- coding: utf-8 -*-

from flask import Flask, render_template, session, escape, url_for, redirect, abort, g
from flask import request as frequest
import requests as prequests
from dateutil import parser
import json
app = Flask(__name__)

headers = {'content-type': 'application/json'}
end_point = 'http://178.62.207.239/notify-api'

@app.template_filter('strftime')
def _jinja2_filter_datetime(date, fmt=None):
    date = parser.parse(date)
    native = date.replace(tzinfo=None)
    format='%d %B %Y'
    return native.strftime(format)

@app.route('/')
def index():
    return render_template('index.html', title='Notify', label='Homepage')

@app.context_processor
def inject_tags():
    url = end_point + '/tags'
    r = prequests.get(url)
    tags_list = r.json()['Roles']
    g.tags = tags_list
    return dict(tags=g.tags)




@app.route('/user')
def user():
    if 'username' not in session:
        return redirect(url_for('login'))

    user = session['username']

    url = end_point + '/posts/user/' + user

    r = prequests.get(url)

    if r.status_code is not 200:
        return abort(500)

    posts = r.json()['Posts']
    for post in posts:
        post['published_at'] = parser.parse(post['published_at'])

    return render_template('user_posts.html', posts=posts, user=user, label=user, title='Posts')

@app.route('/tag/<string:name>')
def tag(name):
    url = end_point + '/posts/tag/'+ name +'/published'

    r = prequests.get(url)

    if r.status_code is not 200:
        return abort(500)

    posts = r.json()['Posts']
    for post in posts:
        post['published_at'] = parser.parse(post['published_at'])


    return render_template('tag_posts.html', tag=name, posts=posts, label=name, title='Posts')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'username' in session:
        return redirect(url_for('user'))

    if frequest.method == 'POST':

        username = escape(frequest.form['username'])
        password = escape(frequest.form['password'])

        url = end_point+'/login'
        payload = {'username':username, 'password':password}

        response = prequests.post(url, data=json.dumps(payload), headers=headers)

        if response.status_code == 200:
            user = response.json()['Profile']
            session['user_id'] = user['id']
            session['username'] = user['username']
            session['user_roles'] = user['roles']
            session['api_cookies'] = dict(response.cookies)

            return redirect(url_for('index'))

        else:
            return abort(500)

    return render_template('login.html')

@app.route('/create', methods=['GET', 'POST'])
def create():
    #sessioncontrol
    if frequest.method == 'POST':
        payload = {
        'username' : session['username'],
        'title' : escape(frequest.form['title']),
        'content' : escape(frequest.form['body']),
        'tag' : escape(frequest.form['tag']),
        'published_at' : escape(frequest.form['published_at'])
        }
        url = end_point + '/create'

        response = prequests.post(url, data=json.dumps(payload), headers=headers, cookies=session['api_cookies'])

        if response.status_code == 201:
            return redirect(url_for('user'))
        else:
            return abort(500)

    u_tags = session['user_roles']

    return render_template('create_post.html', title='Create Post', tags=u_tags)

@app.route('/logout')
def logout():
    if 'username' not in session:
        return redirect(url_for('/login'))

    session.clear()

    return ('', 204)


@app.route('/delete/<int:id>', methods=['DELETE'])
def delete(id):
    url  = end_point + '/posts/'+ str(id)
    response = prequests.delete(url, headers=headers, cookies=session['api_cookies'])

    if response.status_code is not 200:
        return abort(500)

    return ('', 204)

@app.route('/edit/<int:id>', methods=['GET', 'POST'])
def edit(id):
    if frequest.method == 'POST':
        url = end_point + '/posts/' + str(id)

        payload = {
            'username' : session['username'],
            'title' : escape(frequest.form['title']),
            'content' : escape(frequest.form['body']),
            'tag'   : escape(frequest.form['tag']),
            'published_at' : escape(frequest.form['published_at'])
        }

        r = prequests.put(url, data = json.dumps(payload), headers=headers, cookies=session['api_cookies'])

        if r.status_code is not 200:
            return abort(500)

        return redirect(url_for('user'))

    url = end_point + '/posts/' + str(id)
    r = prequests.get(url)

    if r.status_code is not 200:
        return abort(500)

    post = r.json()['Post']

    return render_template('edit_post.html', title='Edit', label=session['username'], tags=session['user_roles'], post=post)


app.secret_key = 'h5XxSLHJXELlz4NAYMja-LpK9RCZx3CE8jXx9jURj'

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
